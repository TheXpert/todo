// server.js

    // set up ========================
    var express  = require('express');
    var app      = express();                               // create our app w/ express
    var morgan = require('morgan');             // log requests to the console (express4)
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
    var fs = require('fs');    

    // configuration =================

    app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());

    var data = JSON.parse(fs.readFileSync("./data/todo.json"));

    var max_id = 0;
    data.forEach(function(item, i, arr) {
        if (item.id > max_id) {
            max_id = item.id;
        }
    });

    app.get('/', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

    app.get('/api/data/', function(req, res) {
        res.sendfile('./data/todo.json'); // load the single view file (angular will handle the page changes on the front-end)
    });

    app.post('/api/data/', function(req, res) {
        var todos = JSON.parse(fs.readFileSync("./data/todo.json"));
        var new_todo = {
            "id": ++max_id,
            "text": req.body.text,
            "done": false
        };

        todos.push(new_todo);
        try {
            fs.writeFileSync("./data/todo.json", JSON.stringify(todos, "", 4).toString());
        }
        catch (err) {
            res.send(err);
        }
        res.sendfile('./data/todo.json'); // load the single view file (angular will handle the page changes on the front-end)
    });

    app.post('/api/data/:todo_id', function(req, res) {
        var todos = JSON.parse(fs.readFileSync("./data/todo.json"));
        
        var id = req.params.todo_id;
        for (var i = 0; i < todos.length; i++) {
            if (todos[i].id == id) {
                todos[i].done = true;
            }
        }

        try {
            fs.writeFileSync("./data/todo.json", JSON.stringify(todos, "", 4).toString());
        }
        catch (err) {
            res.send(err);
        }
        res.sendfile('./data/todo.json'); // load the single view file (angular will handle the page changes on the front-end)
    });

    app.delete('/api/data/:todo_id', function(req, res) {
        var todos = JSON.parse(fs.readFileSync("./data/todo.json"));
        
        var id = req.params.todo_id;
        var index = -1;
        for (var i = 0; i < todos.length; i++) {
            if (todos[i].id == id) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            todos.splice(index, 1);
        }

        try {
            fs.writeFileSync("./data/todo.json", JSON.stringify(todos, "", 4).toString());
        }
        catch (err) {
            res.send(err);
        }
        res.sendfile('./data/todo.json'); // load the single view file (angular will handle the page changes on the front-end)
    });

    // listen (start app with node server.js) ======================================
    app.listen(8080);
    console.log("App listening on port 8080");

